/**
 * @author THANH TRAN
 * @studentno 991515427
 * @program validates password entered by user
 *
 */

import static org.junit.Assert.*;

import org.junit.Test;

public class PassValidatorTest {

	//tests for password to have more than 8 characters
	@Test
	public void testIsValidPasswordConditionOne()
	{
		PassValidator validPass = new PassValidator();
		boolean validResult = validPass.isValidPassword("Co11ege");
		assertFalse("Password needs 8 characters", validResult);
	}

	// test for password to have at least two digits
	@Test
	public void testIsValidPasswordConditionTwo() 
	{
		PassValidator validPass = new PassValidator();
		boolean validResult = validPass.isValidPassword("Sheridan");
		assertFalse("password needs two digits", validResult );
	}
	
	// test that password meets all conditions
	@Test
	public void testIsValidPasswordRegular()
	{
		PassValidator validPass = new PassValidator();
		boolean validResult = validPass.isValidPassword("Sh3r1dAn");
		assertTrue("Password is valid", validResult);
	}
	
	//tests if password is null
	@Test (expected = NullPointerException.class)
	public void testIsValidPasswordException()
	{
		PassValidator validPass = new PassValidator();
		boolean validResult = validPass.isValidPassword(null);
		fail("The password is not valid");
	}
	
	// tests that password works at just 8 characters and 2 digits and has at least 
	// an upper and lowercase letter
	@Test
	public void testIsValidPasswordBoundaryIn()
	{
		PassValidator validPass = new PassValidator();
		boolean validResult = validPass.isValidPassword("Sh3r1dAn");
		assertTrue("Password is valid", validResult);
	}
	
	//tests if password is blank
	@Test 
	public void testIsValidPasswordBoundaryOut()
	{
		PassValidator validPass = new PassValidator();
		boolean validResult = validPass.isValidPassword("");
		assertFalse("The password is not valid", validResult);
	}

}
