import java.util.Scanner;

/**
 * @author THANH TRAN
 * @studentno 991515427
 * @program validates password entered by user
 *
 */

class PassValidator
{
	
	/*
	 * public static void main(String args[]) {
	 * System.out.println("Enter a password"); Scanner input = new
	 * Scanner(System.in); String userPass = input.next();
	 * 
	 * boolean check = isValidPassword(userPass);
	 * 
	 * System.out.println(check); }
	 */
	
	/* Checks if String input contains the following:
	 * is at least 8 or more characters
	 * contains at least 2 digits
	 * has at least one uppercase char
	 * has at least one lowercase char
	 * */
	
	public static boolean isValidPassword(String pass) throws NullPointerException
	{	
		boolean retval = false;
		
		boolean isLongEnough = false;
		boolean hasEnoughDigits = false;
		boolean containsUpper = false;
		boolean containsLower = false; 
		
		//Check for the number of digits
		int numOfDigits = 0;
		
		//convert string into char
		char[] chars = pass.toCharArray();
		
		// create an instance of string builder
		StringBuilder sb = new StringBuilder();
		
		// check each character in the char array and check if there is a digit 
		for (char c: chars)
		{
			if(Character.isDigit(c)) 
			{
				sb.append(c); // append any digits together
			}
			
			if(Character.isLowerCase(c))
			{
				containsLower = true;
			}
					
		}
		
		// check length of what is stored in string builder
		numOfDigits = sb.length();	
		
		if (numOfDigits >=2))
		{
			hasEnoughDigits = true;
		}
		
		//check if password is at least 8 characters long
		if((pass.length) >= 8)
		{
			isLongEnough = true;
		}
		
		// check if string meets password conditions
		if (isLongEnough && hasEnoughDigits && containsLower && containsUppder)
		{
			retVal = true;		
		}
}
